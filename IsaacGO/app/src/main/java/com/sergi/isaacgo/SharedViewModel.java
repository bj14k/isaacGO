package com.sergi.isaacgo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Created by sergi on 9/12/17.
 */

public class SharedViewModel extends ViewModel {

    private final MutableLiveData<Monster> selected = new MutableLiveData<Monster>();

    public LiveData<Monster> getSelected() {
        return selected;
    }

    public void select(Monster monster)
    {
        selected.setValue(monster);
    }





}
