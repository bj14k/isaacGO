package com.sergi.isaacgo;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by sergi on 9/12/17.
 */
@Dao
public interface MonsterDAO {

    @Query("select * from Monster")
    LiveData<List<Monster>> getMonsters();

    @Insert
    void addMonsters(List<Monster> monsters);

    @Insert
    void addMonster(Monster monster);

    @Delete
    void deleteMonster(Monster monster);

    @Query("delete from monster")
    void deleteMonsters();

    @Query("select * from Monster LIMIT 1")
     Monster getMonster();

    @Query("UPDATE Monster SET many = many+1  WHERE id = :cid")
    void catchMonster(int cid);

}
