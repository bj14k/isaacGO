package com.sergi.isaacgo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.sergi.isaacgo.databinding.FragmentMonsterDexBinding;

import java.util.ArrayList;
import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */
public class MonsterDexFragment extends Fragment {

    private FragmentMonsterDexBinding binding;
    private MonsterViewModel model;
    private SharedViewModel sharedModel;
    private ProgressDialog dialog;
    ArrayList<Monster> monsters = new ArrayList<>();;
    private MonsterAdapter adapter;

    public MonsterDexFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentMonsterDexBinding.inflate(inflater);
        View view = binding.getRoot();

        adapter = new MonsterAdapter(
                getContext(), R.layout.fragment_monster_dex, monsters
        );

        binding.dexView.setAdapter(adapter);
        binding.dexView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Monster monsterPos = (Monster) adapterView.getItemAtPosition(i);
                if (monsterPos != null){
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getContext());
                    dlgAlert.setTitle("Has capturado:");
                    dlgAlert.setMessage(String.valueOf(monsterPos.getMany()));
                    dlgAlert.setPositiveButton("Aceptar", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                }
            }
        });

        model = ViewModelProviders.of(this).get(MonsterViewModel.class);


        model.getMonsters().observe(this, new Observer<List<Monster>>() {
            @Override
            public void onChanged(@Nullable List<Monster> monsters) {
                adapter.clear();
                adapter.addAll(monsters);
            }
        });

        dialog = new ProgressDialog(getContext());
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean seMuestra) {
                if (seMuestra) {
                    dialog.show();
                } else {
                    dialog.dismiss();
                }
            }
        });

        model.getMonstersBD();
        
        return view;
    }

}
